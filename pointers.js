var pointUrl = chrome.extension.getURL('/pointer.png');
var pointUrlValue = "url(" + pointUrl + "), auto";

function updatePointers() {
  updatePointer(document.body);
  currentElement && updatePointer(currentElement);
}

function updatePointer(element) {
  if (isIncognitoModeOn) {

    $(element).css({
      "cursor": pointUrlValue
    });
  } else {
    element.style.cursor = '';
  }
}