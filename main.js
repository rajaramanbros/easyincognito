var isIncognitoModeOn = false;
var currentElement;

console.log("Polling for current status");
chrome.runtime.sendMessage({currentConfig: 1}, function (response) {

  isIncognitoModeOn = response.isOn;
  console.log("Poll response is ", isIncognitoModeOn);

  updatePointers();
});

function attach($allLinks) {
  $allLinks.click(function (event) {
    if (isIncognitoModeOn) {
      console.log("Incognito is on... background script will take care of action");
      var target = event.currentTarget || event.target;
      var message = {
        "url": (target.href || target.baseURI), "incognito": true
      };

      console.log("Sending message");
      console.log(message);

      chrome.runtime.sendMessage(message);

      event.preventDefault();
      event.stopPropagation();
    }
  });

  $allLinks.mouseenter(function (event) {
    currentElement = event.target;
    updatePointers();
  });

}
function detach($allLinks) {
  $allLinks.off('click');
  $allLinks.off('mouseenter');
}

$(document).ready(function () {

  var allLinks;

  setInterval(function () {
    if (allLinks) {
      detach(allLinks);
    }

    allLinks = $('a');
    attach(allLinks);
  }, 1000);

  chrome.runtime.onMessage.addListener(function (request) {
    console.log("Received broadcast", request);

    if (request.commandReceived) {
      isIncognitoModeOn = "incognito-on" == request.commandReceived;
      updatePointers();
    }
  });

});