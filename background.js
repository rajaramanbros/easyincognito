var createdWindow;

function newTab(url) {
  chrome.tabs.create({
    windowId: createdWindow.id, url: url
  }, function (response) {
    if (!response) {
      newWindow(url);
    }
  });
}

function newWindow(url) {
  chrome.windows.create({
    "url": url, "incognito": true
  }, function (window) {
    createdWindow = window;
    console.log(createdWindow);
  });
}

function newIncognito(url) {
  if (!createdWindow) {
    newWindow(url);
  } else {
    newTab(url);
  }
}

function broadcast(command) {
  console.log("Broadcasting to all tabs", command);

  chrome.tabs.query({}, function (tabs) {
    for (var i = 0; i < tabs.length; i++) {
      chrome.tabs.sendMessage(tabs[i].id, {
        "commandReceived": command
      });
    }
  });
}

var lastCommandReceived;

chrome.commands.onCommand.addListener(function (command) {
  lastCommandReceived = command;
  broadcast(command);
  changeBrowserActionIcon(command);
});

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.url) {
    newIncognito(request.url);
  }

  if (request.currentConfig) {
    sendResponse({isOn: lastCommandReceived == "incognito-on"});
  }
});

chrome.browserAction.onClicked.addListener(function () {
  lastCommandReceived = lastCommandReceived == "incognito-on" ? "incognito-off" : "incognito-on";
  broadcast(lastCommandReceived);
  changeBrowserActionIcon(lastCommandReceived);
});

function changeBrowserActionIcon(command) {
  console.log(command);

  var picturePath = command == "incognito-on" ? '/locked-128.png' : '/unlocked-128.png';
  chrome.browserAction.setIcon({
    path: picturePath
  });
}
